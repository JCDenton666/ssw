<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title>TSWATG: Spreadsheet Wars</title>

    
</head>

<body onload="update(6); createInv()">
    <div id="flexAll">
        <div id="flexTop">
            <div id="flex0">
                <div id="leftBar">
                    <div class="equipArea"><strong>Equipped:</strong></div>
                    <div class="equipArea">Right hand: <span id="equippedWeapon1"></span></div>
                    <div class="equipArea">Left hand: <span id="equippedWeapon2"></span></div>
                    <!-- <div class="equipArea">Dual wielding: <button id="dualWieldingToggle">Off</button></div> -->
                    <div class="equipArea">Head: <span id="equippedHelmet"></span></div>
                    <div class="equipArea">Torso: <span id="equippedTorso"></span></div>
                    <div class="equipArea">Back item: <span id="equippedBack"></span></div>
                    <div class="equipArea">Pants: <span id="equippedPants"></span></div>
                    <div class="equipArea">Accessory 1: <span id="equippedAccessory1"></span></div>
                    <div class="equipArea">Accessory 2: <span id="equippedAccessory2"></span></div>
                    <div class="equipArea">Accessory 3: <span id="equippedAccessory3"></span></div>
                    <div class="equipArea">Accessory 4: <span id="equippedAccessory4"></span></div>
                    <ul id="statBar">
                        <li>Name: <span id="playerName"></span></li>
                        <li>Sex: <span id="playerSex"></span></li>
                        <li>Class: <span id="playerClass"></span></li></li>
                        <li>Level: <span id="levelText">1</span></li>
                        <li>STR: <span id="str"></span></li>
                        <li>DEX: <span id="dex"></span></li>
                        <li>CON: <span id="con"></span></li>
                        <li>INT: <span id="int"></span></li>
                        <li>WIS: <span id="wis"></span></li>
                        <li>CHA: <span id="cha"></span></li>
                        <li>Armour class: <span id="acText"></span></li>
                        <li>Health: <span id="healthText"></span>/<span id="maxHealthText"></li>
                        <li>Mana: <span id="manaText"></span>/<span id="maxManaText"></li>
                        <li>Stamina: <span id="staminaText"></span>/<span id="maxStaminaText"></li>
                        <li>Yen: <span id="yenText"></span></li>
                        <li>Turns left: <span id="turns"></span></li>
                        </li>
                        <li>Buffs:</li>
                        <li>Debuffs:</li>
                        <li>Continuous effects:</li>
                        </ul>
                </div>

            </div>


            <div id="flex1">
                <ul>
                    <li id="character" class="topBar">Character</li>
                    <li id="questLog" class="topBar notyet">Quest log</li>
                    <li id="inventory" class="topBar">Inventory</li>
                    <li id="auctionHouse" class="topBar notyet">Auction house</li>
                    <li id="crafting" class="topBar notyet">Crafting</li>
                    <li id="research" class="topBar notyet">Research</li>
                    <li id="monstermanual" class="topBar notyet">Monster manual</li>
                    <li id="options" class="topBar notyet">Options</li><br>
                    <li id="zone1" class="topBar">Your room</li>
                    <li id="zone2" class="topBar">Dream</li>
                    <li id="zone3" class="topBar notyet">Quickzone 3</li>
                    <li id="zone4" class="topBar notyet">Quickzone 4</li>
                    <li id="zone5" class="topBar notyet">Quickzone 5</li>
                </ul>
            </div>
            <div id="flex2">
                <div id="rightBar">
                    <ul>
                        <li class="monsterProp">Monster name: <span id="monsterName2"></span></li>
                        <li class="monsterProp">Type: <span id="monsterType"></span></li>
                        <li class="monsterProp">Health: <span id="monsterHealth2"></span></li>
                        <li class="monsterProp">Armor class: <span id="monsterAc"></span></li>
                        <li class="monsterProp">Item drops: <span id="monsterItems"></span></li>
                        <li class="monsterProp">Yen drop: <span id="monsterYen"></span></li>
                        <li class="monsterProp debug">InitRounder: <span id="initRounder"></span></li>
                        <li class="monsterProp debug">Init: <span id="playerInit"></span></li>
                        <li class="monsterProp debug">Monster init: <span id="monsterInit"></span></li>
                        <br>
                        <li id="itemValues"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
        <!-- <div id="flex3">
    <div id="monsterProperties">
    <ul>
        
    </ul>
    </div>
    </div> -->
        <div id="flex4">
            <div id="characterAll" class="menus">
                <img src="Stuff/ainafront.jpg">
                <ul id="characterList">
                <li class="characterMenuStats">Name: <span id="playerNameC"></span></li>
                <li class="characterMenuStats">Sex: <span id="playerSexC"></span></li>
                        <li class="characterMenuStats">Class: <span id="playerClassC"></span></li></li>
                        <li class="characterMenuStats">Level: <span id="levelTextC"></span></li>
                        <li class="characterMenuStats">STR: <span id="strC"></span></li>
                        <li class="characterMenuStats">DEX: <span id="dexC"></span></li>
                        <li class="characterMenuStats">CON: <span id="conC"></span></li>
                        <li class="characterMenuStats">INT: <span id="intC"></span></li>
                        <li class="characterMenuStats">WIS: <span id="wisC"></span></li>
                        <li class="characterMenuStats">CHA: <span id="chaC"></span></li>
                        <br>
                        <li class="characterMenuStatsSmall">Health: <span id="healthTextC"></span>/<span id="maxHealthTextC"></li>
                            <li class="characterMenuStatsSmall">Mana: <span id="manaTextC"></span>/<span id="maxManaTextC"></li>
                            <li class="characterMenuStatsSmall">Stamina: <span id="staminaTextC"></span>/<span id="maxStaminaTextC"></li><br><br>
                                <li class="characterMenuStatsSmall"><strong>Skills:</strong> <span id="skillsC"></span></li>
                </ul>
            </div>
            <div id="inventoryAll" class="menus"></div>
            <div id="titleNC"></div>

            <div id="mapHolder">
                <div id="mapImage">
                </div>
            
            <div id="bigImage">
            </div>
        </div>
            <!-- Move the closing div here -->
            <div id="game">
                <!-- <div id="stats">
                        <span class="stat">Dice roll: <strong><span id="d20Text"></span></strong></span>
        </div> -->
                <!-- <div id="controls">
            
        </div> -->


                <div id="monsterStats">
                    <span class="stat">Monster Name: <strong><span id="monsterName"></span></strong></span>
                    <span class="stat">Health: <strong><span id="monsterHealth"></span></strong></span>
                    <span class="stat">Die roll: <strong><span id="d20Text"></strong></span>
                    <span class="stat">Attack roll: <strong><span id="atkRoll"></strong></span>
                    <span class="stat">Damage roll: <strong><span id="damRoll"></strong></span>
                </div>
                <div id="intro">TSWATG: Spreadsheet Wars<br>
                    <p>You wake up and, after a quick look at your alarm clock, hit it to turn off the alarm function. Today
                        is the first day of your first year of middle school, and though you don't want to go to school,
                        much less leave your house, even much less leave your room, and least of all leave your bed, you
                        still have to, if only to avoid your foster mother Tomiko's incessant nagging. You pull the blanket
                        over your head and consider simulating to be sick, but eventually decide against it.</p>
                    <p>As you get out of bed, you sense that something is odd, that this day is not like any other that came
                        before it. Maybe it's the heat, maybe it's growing up, maybe it's the LSD in the water supply… but
                        today things feel a lot more surreal than usual, and you feel like dealing with them in a much more
                        belligerent way.</p>
                    <p>You use the toilet in your bathroom and put on your underwear, starting with your left sock, and
                        finishing with your right (disclaimer: for the purpose of this adventure, socks are underwear). You
                        brush your teeth, and your hair (note: at age 12, you're supposed to take care of mundane things
                        like that yourself, so I won't tell you about them in the future, unless something out of the
                        ordinary happens while you do them). Then you sigh. Your foster mother (you never forget the
                        “foster” part, and any attempts by her to make you abandon it usually result in screaming matches)
                        mentioned something about chores yesterday evening. You better gather the courage to go down and
                        find out what she wants you to do before you can start your “new” and “exciting” middle school life.
                        You sigh again.</p>
                    <p>You look at your calendar. It's September 4th, 1999.</p>
                </div>
                <div id="runText"></div>
                <div id="initText"></div>
                <div id="text"></div>         
                <div id="beyondText"></div> 
                <button id="afterText">Continue</button>
                <div id="buttonContainer">
                    <div class="buttonRow">
                        <button id="button1" class="combatButtons">Use skill</button>
                        <select id="selectSkill1" class="combatButtons" name="skills">
                            <option value="Default">Default</option>
                        </select>
                        <button id="button3" class="combatButtons">Use item</button>
                        <select id="selectItem1" class="combatButtons" name="items">
                            <option value="Default">Default</option>
                        </select>
                    </div>
                    <div class="buttonRow">
                        <button id="button2" class="combatButtons">Use skill 2</button>
                        <select id="selectSkill2" class="combatButtons"><option value="Default">Default</option></select>
                        <button id="button4" class="combatButtons">Use item 2</button>
                          <select id="selectItem2" class="combatButtons"><option value="Default">Default</option></select>
                    </div>
                    <div class="buttonRow">
                        <button id="button5" class="combatButtons">Run</button>
                        <button id="button6" class="combatButtons">Attack</button>
                    </div>
                </div>
                <button id="button7" class="afterCombatButtons">Adventure again</button></br>
                <button id="button8" class="afterCombatButtons">Go to your room</button>
            </div>
        </div>
    
    <!-- <img src="room.jpg" id="room"> -->
    <div id="title"></div>

    <div id="description"></div>
    
   
    <?php


include_once 'setup.php';





// echo '<script>';
// echo 'var maps = ' . json_encode($maps) . ';';
// echo '</script>';


echo '<script>';
echo 'var items = ' . json_encode($items) . ';'; 
echo '</script>';

echo '<script>';
    echo 'var monsters = ' . json_encode($monsters) . ';';
    echo '</script>';

    echo '<script>';
    echo 'var skills = ' . json_encode($skills) . ';';
      echo '</script>';

echo '<script type="text/javascript">';
readfile('script.js'); // Output the contents of script.js
echo '</script>';



?>




    


    
</body>

</html>