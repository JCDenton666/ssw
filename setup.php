<?php

require_once('databaseconfig.php');

// Create an instance of DatabaseConfig with your database parameters
$config = new DatabaseConfig("localhost", "root", "", "ssw");

// Database connection parameters
$servername = $config->getServername();
$username = $config->getUsername();
$password = $config->getPassword();
$dbname = $config->getDbname();

try {
    // Connect to the database
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Define the SQL query to create the Maps table if it doesn't exist
    $sqlCreateMapsTable = "CREATE TABLE IF NOT EXISTS maps (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(255) UNIQUE,
        text TEXT,
        mapArray TEXT,
        mapFunctionsArray TEXT,
        a INT,
        aProb FLOAT,
        b INT,
        bProb FLOAT,
        c INT,
        cProb FLOAT,
        d INT,
        dProb FLOAT
    )";

    // Execute the SQL query to create the Maps table
    $conn->exec($sqlCreateMapsTable);

    // Include the file containing maps data
    require_once('maps.php');

    // Prepare the SQL statement for inserting maps
    $stmt = $conn->prepare("INSERT INTO maps (name, text, mapArray, mapFunctionsArray, a, aProb, b, bProb, c, cProb, d, dProb) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE
text = VALUES(text),
mapArray = VALUES(mapArray),
mapFunctionsArray = VALUES(mapFunctionsArray),
a = VALUES(a),
aProb = VALUES(aProb),
b = VALUES(b),
bProb = VALUES(bProb),
c = VALUES(c),
cProb = VALUES(cProb),
d = VALUES(d),
dProb = VALUES(dProb)");

    // Insert each map into the database
    foreach ($maps as $index => $map) {
        // Set default values for missing keys
        $name = isset($map['name']) ? $map['name'] : '';
        $text = isset($map['text']) ? $map['text'] : '';
        $mapArray = isset($map['mapArray']) ? json_encode($map['mapArray']) : '';
        $mapFunctionsArray = isset($map['mapFunctionsArray']) ? json_encode($map['mapFunctionsArray']) : '';
        $a = isset($map['a']) ? $map['a'] : 0;
        $aProb = isset($map['aProb']) ? $map['aProb'] : 0.0;
        $b = isset($map['b']) ? $map['b'] : 0;
        $bProb = isset($map['bProb']) ? $map['bProb'] : 0.0;
        $c = isset($map['c']) ? $map['c'] : 0;
        $cProb = isset($map['cProb']) ? $map['cProb'] : 0.0;
        $d = isset($map['d']) ? $map['d'] : 0;
        $dProb = isset($map['dProb']) ? $map['dProb'] : 0.0;
    
        // Execute the SQL statement to insert map data
        $stmt->execute([$name, $text, $mapArray, $mapFunctionsArray, $a, $aProb, $b, $bProb, $c, $cProb, $d, $dProb]);
    }
    
    // echo "Maps inserted successfully";

    // Fetch the inserted maps data from the database
    $stmt = $conn->query("SELECT * FROM maps");
    $mapsData = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // Convert the fetched data into JSON using json_encode
    $maps = [];
    foreach ($mapsData as $mapData) {
        // Convert mapArray from string to array
        $mapData['mapArray'] = json_decode($mapData['mapArray']);
        $maps[] = $mapData;
    }

    $mapsJSON = json_encode($maps, JSON_UNESCAPED_SLASHES);

    // Output JavaScript code to initialize a variable with the JSON data without double quotes
    echo '<script>';
    echo 'var maps = ' . preg_replace('/"(mapArray|mapFunctionsArray)":/', '$1:', $mapsJSON) . ';';
    echo '</script>';

} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

// Close connection
$conn = null;




try {
    // Connect to the database
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Define the SQL query to create the Items table if it doesn't exist
    $sqlCreateItemsTable = "CREATE TABLE IF NOT EXISTS items (
        id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(255) UNIQUE,
        amount INT,
        category VARCHAR(255),
        type VARCHAR(255),
        subtype VARCHAR(255),
        handedness VARCHAR(255),
        attackBonus INT,
        damageType VARCHAR(255),
        damageDie INT,
        damageBonus INT,
        acBonus INT,
        wisBonus INT,
        intBonus INT,
        descr TEXT,
        adventures INT,
        fullness INT,
        strGain FLOAT,
        subcategory VARCHAR(255)
    )";

    // Execute the SQL query to create the Items table
    $conn->exec($sqlCreateItemsTable);

    // Include the file containing items data
    require_once('items.php');

    // Prepare the SQL statement for inserting items
    $stmt = $conn->prepare("INSERT INTO items (name, amount, category, type, subtype, handedness, attackBonus, damageType, damageDie, damageBonus, acBonus, wisBonus, intBonus, descr, adventures, fullness, strGain, subcategory) 
        SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
        FROM dual
        WHERE NOT EXISTS (SELECT 1 FROM items WHERE name = ?)");

    // Insert each item into the database
    foreach ($items as $item) {
        $stmt->execute([
            $item['name'] ?? null,
            $item['amount'] ?? null,
            $item['category'] ?? null,
            $item['type'] ?? null,
            $item['subtype'] ?? null,
            $item['handedness'] ?? null,
            $item['attackBonus'] ?? null,
            $item['damageType'] ?? null,
            $item['damageDie'] ?? null,
            $item['damageBonus'] ?? null,
            $item['acBonus'] ?? null,
            $item['wisBonus'] ?? null,
            $item['intBonus'] ?? null,
            $item['descr'] ?? null,
            $item['adventures'] ?? null,
            $item['fullness'] ?? null,
            $item['strGain'] ?? null,
            $item['subcategory'] ?? null,
            $item['name'] ?? null
        ]);
    }

    // echo "Items inserted successfully";

} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}






// Include the file containing skills data
require_once('skills.php');


try {
    // Connect to MySQL database (using PDO)
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // Set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Define the SQL query to create the Skills table if it doesn't exist
    $sql = "CREATE TABLE IF NOT EXISTS Skills (
        id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(255) UNIQUE,
        learned BOOLEAN,
        effect1 VARCHAR(20),
        effect1Variable SMALLINT(10),
        costType1 VARCHAR(20),
        cost1 SMALLINT(10)
    )";
    // Execute the SQL query
    $conn->exec($sql);

    // Prepare the SQL statement for inserting skills
    $stmt = $conn->prepare("INSERT INTO Skills (name, learned, effect1, effect1Variable, costType1, cost1) VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE
    learned = VALUES(learned),
    effect1 = VALUES(effect1),
    effect1Variable = VALUES(effect1Variable),
    costType1 = VALUES(costType1),
    cost1 = VALUES(cost1)");

foreach ($skills as $skill) {
$stmt->execute([
$skill['name'] ?? null,
$skill['learned'] ?? null,
$skill['effect1'] ?? null,
$skill['effect1Variable'] ?? null,
$skill['costType1'] ?? null,
$skill['cost1'] ?? null
]);
}

    // echo "Skills inserted successfully";

} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}





// Include the file containing monsters data
require_once('monsters.php');



try {
    // Connect to MySQL database (using PDO)
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // Set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Define the SQL query to create the Monsters table if it doesn't exist
    $sql = "CREATE TABLE IF NOT EXISTS Monsters (
        id VARCHAR(10) PRIMARY KEY,
        name VARCHAR(255),
        indef VARCHAR(10),
        type VARCHAR(255),
        level INT,
        health INT,
        ac INT,
        atk INT,
        dam INT,
        init INT,
        statgain INT,
        drop1name VARCHAR(255),
        drop1 VARCHAR(10),
        dropprob1 FLOAT,
        drop2name VARCHAR(255),
        drop2 VARCHAR(10),
        dropprob2 FLOAT,
        drop3name VARCHAR(255),
        drop3 VARCHAR(10),
        dropprob3 FLOAT,
        drop4name VARCHAR(255),
        drop4 VARCHAR(10),
        dropprob4 FLOAT,
        drop5name VARCHAR(255),
        drop5 VARCHAR(10),
        dropprob5 FLOAT,
        yen INT
    )";
    // Execute the SQL query to create the table
    $conn->exec($sql);

    // Prepare the SQL statement for inserting monsters
    $stmt = $conn->prepare("INSERT IGNORE INTO Monsters (id, name, indef, type, level, health, ac, atk, dam, init, statgain, drop1name, drop1, dropprob1, drop2name, drop2, dropprob2, drop3name, drop3, dropprob3, drop4name, drop4, dropprob4, drop5name, drop5, dropprob5, yen) VALUES (:id, :name, :indef, :type, :level, :health, :ac, :atk, :dam, :init, :statgain, :drop1name, :drop1, :dropprob1, :drop2name, :drop2, :dropprob2, :drop3name, :drop3, :dropprob3, :drop4name, :drop4, :dropprob4, :drop5name, :drop5, :dropprob5, :yen)");

    // Insert each monster into the database
    foreach ($monsters as $monster) {
        $stmt->bindParam(':id', $monster['id']);
        $stmt->bindParam(':name', $monster['name']);
        $stmt->bindParam(':indef', $monster['indef']);
        $stmt->bindParam(':type', $monster['type']);
        $stmt->bindParam(':level', $monster['level']);
        $stmt->bindParam(':health', $monster['health']);
        $stmt->bindParam(':ac', $monster['ac']);
        $stmt->bindParam(':atk', $monster['atk']);
        $stmt->bindParam(':dam', $monster['dam']);
        $stmt->bindParam(':init', $monster['init']);
        $stmt->bindParam(':statgain', $monster['statgain']);
        $stmt->bindParam(':drop1name', $monster['drop1name']);
        $stmt->bindParam(':drop1', $monster['drop1']);
        $stmt->bindParam(':dropprob1', $monster['dropprob1']);
        $stmt->bindParam(':drop2name', $monster['drop2name']);
        $stmt->bindParam(':drop2', $monster['drop2']);
        $stmt->bindParam(':dropprob2', $monster['dropprob2']);
        $stmt->bindParam(':drop3name', $monster['drop3name']);
        $stmt->bindParam(':drop3', $monster['drop3']);
        $stmt->bindParam(':dropprob3', $monster['dropprob3']);
        $stmt->bindParam(':drop4name', $monster['drop4name']);
        $stmt->bindParam(':drop4', $monster['drop4']);
        $stmt->bindParam(':dropprob4', $monster['dropprob4']);
        $stmt->bindParam(':drop5name', $monster['drop5name']);
        $stmt->bindParam(':drop5', $monster['drop5']);
        $stmt->bindParam(':dropprob5', $monster['dropprob5']);
        $stmt->bindParam(':yen', $monster['yen']);
        
        $stmt->execute();
    }

    

    // echo "Monsters inserted successfully";

} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

// Close connection
$conn = null;

