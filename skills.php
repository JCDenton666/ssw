<?php
// Define the skills data
// $skills = [
//     ["sk1", "Colour vision", true, "st0", null],
//     ["sk2", "Armour weariness", false, "st1", null],
//     ["sk3", "Hit", false, "damage", ""],
//     ["sk4", "Toughness", false, "hp", "5"],
//     ["sk5", "Improved initiative", false, "initBonus", "2"],
//     ["sk6", "Shield bash", true, "damage", ""],
//     ["sk7", "Double hit", false, "damage", ""]
// ];

$skills = [
    ['id' => 'sk0', 'name' => 'Dummy', 'learned' => false, 'type' => 'game', 'effect1' => 'st0'],
    ['id' => 'sk1', 'name' => 'Colour vision', 'learned' => true, 'type' => 'game', 'effect1' => 'st1'],
    ['id' => 'sk2', 'name' => 'Armour weariness', 'learned' => false, 'type' => 'game', 'effect1' => 'st2'],
    ['id' => 'sk3', 'name' => 'Hit', 'learned' => true, 'type' => 'combat', 'effect1' => 'damage', 'effect1Variable' => 2, 'costType1' => 'stamina', 'cost1' => 2],
    ['id' => 'sk4', 'name' => 'Toughness', 'learned' => false, 'type' => 'game', 'effect1' => 'hp', 'effect1Variable' => 5],
    ['id' => 'sk5', 'name' => 'Improved initiative', 'learned' => false, 'type' => 'game', 'effect1' => 'initBonus', 'effect1Variable' => 2],
    ['id' => 'sk6', 'name' => 'Shield bash', 'learned' => false, 'effect1' => 'damage', 'effect1Variable' => ''],
    ['id' => 'sk7', 'name' => 'Double hit', 'learned' => false, 'effect1' => 'damage', 'effect1Variable' => ''],
    ['id' => 'sk8', 'name' => 'Heal laughable wound', 'learned' => false, 'type' => 'hybrid', 'effect1' => 'hp', 'effect1Variable' => 1],
    ['id' => 'sk9', 'name' => 'Mage hand', 'learned' => true, 'type' => 'combat', 'effect1' => 'damage', 'effect1Variable' => 2, 'costType1' => 'mana', 'cost1' => 2]
];

?>