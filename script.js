let level = 1;
let levelBonus = 0;
let health;
let maxHealth;
let mana;
let maxMana;
let stamina;
let maxStamina;
let yen = 4;
let currentWeapon = 0;
let attackBonus;
let damage;
let ac = 10;
let initBonus = 0;
let fighting;
let monsterHealth;
let turns = 200;
var curmon_helper;
var curMon;
var curMonActive = false;
// var dreamget;
var monsterInitWon;
var initRound = true;
let introShown = false;
var fightOn;
var inventoryShown = false;


var equippedWeapon1;
var equippedWeapon2;
var equippedShield;
var equippedHelmet;
var equippedTorso;
var equippedBack;
var equippedPants;
var equippedAccessory1;
var equippedAccessory2;
var equippedAccessory3;
var equippedAccessory4;

var currentArea;
var mapCreated = false;
var mapHolderTable;
var selectedAction;

var dualWielding = false;


var equippedWeapon1Id = 0;
var equippedWeapon1Type = 0;
var equippedWeapon1AttackBonus = 0;
var equippedWeapon1DamageType = 0;
var equippedWeapon1DamageDie = 0;
var equippedWeapon1DamageBonus = 0;
var equippedWeapon1Handedness = 0;
var equippedWeapon2Id = 0;
var equippedWeapon2Name = 0;
var equippedWeapon2Amount = 0;
var equippedWeapon2Category = 0;
var equippedWeapon2Type = 0;
var equippedWeapon2Subtype = 0;
var equippedWeapon2Handedness = 0;
var equippedWeapon2AttackBonus = 0;
var equippedWeapon2DamageType = 0;
var equippedWeapon2DamageDie = 0;
var equippedWeapon2DamageBonus = 0;
var equippedWeapon2Descr = 0;
var equippedShieldId = 0;
var equippedShieldName = 0;
var equippedShieldAmount = 0;
var equippedShieldCategory = 0;
var equippedShieldType = 0;
var equippedShieldAcBonus = 0;
var equippedShieldDamageType = 0;
var equippedShieldDamageDie = 0;
var equippedShieldDamageBonus = 0;
var equippedShieldDescr = 0;
var equippedHelmetId = 0;
var equippedHelmetName = 0;
var equippedHelmetAmount = 0;
var equippedHelmetCategory = 0;
var equippedHelmetType = 0;
var equippedHelmetChaBonus = 0;
var equippedHelmetDescr = 0;
var equippedTorsoId = 0;
var equippedTorsoName = 0;
var equippedTorsoAmount = 0;
var equippedTorsoCategory = 0;
var equippedTorsoType = 0;
var equippedTorsoSubtype = 0;
var equippedTorsoAttackBonus = 0;
var equippedTorsoAcBonus = 0;
var equippedTorsoWisBonus = 0;
var equippedTorsoIntBonus = 0;
var equippedTorsoDescr = 0;
var equippedBackId = 0;
var equippedBackName = 0;
var equippedBackAmount = 0;
var equippedBackCategory = 0;
var equippedBackType = 0;
var equippedBackAcBonus = 0;
var equippedBackIntBonus = 0;
var equippedBackDescr = 0;
var equippedPantsId = 0;
var equippedPantsName = 0;
var equippedPantsAmount = 0;
var equippedPantsCategory = 0;
var equippedPantsType = 0;
var equippedPantsAcBonus = 0;
var equippedPantsIntBonus = 0;
var equippedPantsDescr = 0;
var equippedAccessory1Name = 0;
var equippedAccessory1Amount = 0;
var equippedAccessory1Category = 0;
var equippedAccessory1Type = 0;
var equippedAccessory1IntBonus = 0;
var equippedAccessory1Desc = 0;
var equippedAccessory2Name = 0;
var equippedAccessory2Amount = 0;
var equippedAccessory2Category = 0;
var equippedAccessory2Type = 0;
var equippedAccessory2IntBonus = 0;
var equippedAccessory2Desc = 0;
var equippedAccessory3Name = 0;
var equippedAccessory3Amount = 0;
var equippedAccessory3Category = 0;
var equippedAccessory3Type = 0;
var equippedAccessory3IntBonus = 0;
var equippedAccessory3Desc = 0;
var equippedAccessory4Name = 0;
var equippedAccessory4Amount = 0;
var equippedAccessory4Category = 0;
var equippedAccessory4Type = 0;
var equippedAccessory4IntBonus = 0;
var equippedAccessory4Desc = 0;



const button1 = document.querySelector('#button1');
const button2 = document.querySelector("#button2");
const button3 = document.querySelector("#button3");
const button4 = document.querySelector("#button4");
const button5 = document.querySelector("#button5");
const button6 = document.querySelector("#button6");
const button7 = document.querySelector("#button7");
const button8 = document.querySelector("#button8");
const character = document.querySelector("#character");
const buttonContainer = document.querySelectorAll("#buttonContainer");
const combatButtons = document.querySelectorAll(".combatButtons");
const afterCombatButtons = document.querySelectorAll(".afterCombatButtons");
const menus = document.querySelectorAll(".menus");
const equippedWeapon1Text = document.querySelector("#equippedWeapon1");
const equippedWeapon2Text = document.querySelector("#equippedWeapon2");
const equippedShieldText = document.querySelector("#equippedShield");
const dualWieldingToggle = document.querySelector("#dualWieldingToggle");
const equippedHelmetText = document.querySelector("#equippedHelmet");
const equippedTorsoText = document.querySelector("#equippedTorso");
const equippedBackText = document.querySelector("#equippedBack");
const equippedPantsText = document.querySelector("#equippedPants");
const equippedAccessory1Text = document.querySelector("#equippedAccessory1");
const equippedAccessory2Text = document.querySelector("#equippedAccessory2");
const equippedAccessory3Text = document.querySelector("#equippedAccessory3");
const equippedAccessory4Text = document.querySelector("#equippedAccessory4");
const titleNC = document.querySelector("#titleNC");
const text = document.querySelector("#text");
const beyondText = document.querySelector("#text");
const runText = document.querySelector("#initText");
const initText = document.querySelector("#initText");
const afterText = document.querySelector("#afterText");
const playerNameText = document.querySelector("#playerName");
const playerSexText = document.querySelector("#playerSex");
const playerClassText = document.querySelector("#playerClass");
const levelText = document.querySelector("#levelText");
const strText = document.querySelector("#str");
const dexText = document.querySelector("#dex");
const conText = document.querySelector("#con");
const intText = document.querySelector("#int");
const wisText = document.querySelector("#wis");
const chaText = document.querySelector("#cha");
const acText = document.querySelector("#acText");
const healthText = document.querySelector("#healthText");
const maxHealthText = document.querySelector("#maxHealthText");
const manaText = document.querySelector("#manaText");
const maxManaText = document.querySelector("#maxManaText");
const staminaText = document.querySelector("#staminaText");
const maxStaminaText = document.querySelector("#maxStaminaText");
const playerNameC = document.querySelector("#playerNameC");
const playerSexC = document.querySelector("#playerSexC");
const playerClassC = document.querySelector("#playerClassC");
const levelTextC = document.querySelector("#levelTextC");
const strTextC = document.querySelector("#strC");
const dexTextC = document.querySelector("#dexC");
const conTextC = document.querySelector("#conC");
const intTextC = document.querySelector("#intC");
const wisTextC = document.querySelector("#wisC");
const chaTextC = document.querySelector("#chaC");
const healthTextC = document.querySelector("#healthTextC");
const maxHealthTextC = document.querySelector("#maxHealthTextC");
const manaTextC = document.querySelector("#manaTextC");
const maxManaTextC = document.querySelector("#maxManaTextC");
const staminaTextC = document.querySelector("#staminaTextC");
const maxStaminaTextC = document.querySelector("#maxStaminaTextC");
const skillsC = document.querySelector("#skillsC");
const yenText = document.querySelector("#yenText");
const d20Text = document.querySelector("#d20Text");
const atkRoll = document.querySelector("#atkRoll");
const damRoll = document.querySelector("#damRoll");
const monsterStats = document.querySelector("#monsterStats");
const monsterName = document.querySelector("#monsterName");
const monsterHealthText = document.querySelector("#monsterHealth");
const monsterProperties = document.querySelector("#monsterProperties")
const room = document.querySelector("#room");
const map = document.querySelector("#map");
const enemy = document.querySelectorAll('.enemy');
const image = document.querySelector('#image');
const intro = document.querySelector('#intro');
const monsterName2 = document.querySelector("#monsterName2");
const monsterType = document.querySelector("#monsterType");
const monsterHealth2 = document.querySelector("#monsterHealth2");
const monsterAc = document.querySelector("#monsterAc");
const monsterItems = document.querySelector("#monsterItems");
const monsterYen = document.querySelector("#monsterYen");
const initRounder = document.querySelector("#initRounder");
const playerInit = document.querySelector("#playerInit");
const monsterInit = document.querySelector("#monsterInit");
const characterMenu = document.querySelector("#character");
const characterAll = document.querySelector("#characterAll");
const inventoryMenu = document.querySelector("#inventory");
const inventoryAll = document.querySelector("#inventoryAll");
const turnsText = document.querySelector("#turns");
const zone1 = document.querySelector("#zone1");
const zone2 = document.querySelector("#zone2");
const zone3 = document.querySelector("#zone3");
const mapHolder = document.querySelector("#mapHolder");
const game = document.querySelector("#game");




let str = parseFloat(1.0.toFixed(2));
let dex = parseFloat(1.0.toFixed(2));
let con = parseFloat(1.0.toFixed(2));
let int = parseFloat(1.0.toFixed(2));
let wis = parseFloat(1.0.toFixed(2));
let cha = parseFloat(3.0.toFixed(2));

// Update HTML elements
strText.innerHTML = str.toFixed(2);
dexText.innerHTML = dex.toFixed(2);
conText.innerHTML = con.toFixed(2);
intText.innerHTML = int.toFixed(2);
wisText.innerHTML = wis.toFixed(2);
chaText.innerHTML = cha.toFixed(2);

// Update HTML elements with "C" suffix
levelTextC.innerHTML = level; // Assuming level is defined elsewhere
strTextC.innerHTML = str.toFixed(2);
dexTextC.innerHTML = dex.toFixed(2);
conTextC.innerHTML = con.toFixed(2);
intTextC.innerHTML = int.toFixed(2);
wisTextC.innerHTML = wis.toFixed(2);
chaTextC.innerHTML = cha.toFixed(2);

let strBonus = 0;
let dexBonus = 0;
let conBonus = 0;
let intBonus = 0;
let wisBonus = 0;
let chaBonus = 0;

let healthBonus = 0;
let manaBonus = 0;
let staminaBonus = 0;


let playerName = "Aina Susieno";
playerNameText.innerHTML = playerName;
let playerSex = "Female";
playerSexText.innerHTML = playerSex;
let playerClass = "Fighter";
playerClassText.innerHTML = playerClass;
let classPrimaryAttribute = str;

acText.innerHTML = ac;
yenText.innerHTML = yen;
turnsText.innerHTML = turns;

attackBonus += Math.floor(str/2);

maxHealth = 10 + Math.floor(con * 2);
maxHealthText.innerHTML = maxHealth;

maxMana = 10 + Math.floor(int + wis + cha);
maxManaText.innerHTML = maxMana;

maxStamina = 10 + Math.floor(con * 2);
maxStaminaText.innerHTML = maxStamina;


health = 12
healthText.innerText = health;

mana = 15;
manaText.innerText = mana;

stamina = 12;
staminaText.innerText = stamina;

initRounder.innerText = "true";


characterMenu.onclick = showCharacter;
inventoryMenu.onclick = showInventory;
zone1.onclick = update.bind(null,4);
zone2.onclick = update.bind(null,6);




// healthText.innerHTML = health;
const descriptions = ["What are you clicking? There's nothing there!", "Your wardrobe", "Window", "Calendar", "Desk", "Record player"];






let oldId = null;

equipItemStart(0);
// createMap(6);


function update(areaId) {

  if (areaId == 0 || areaId == 1 || areaId == 2 && mapHolderTable !== null){
    mapHolderTable = document.querySelector('#mapHolder > table');
    console.log(mapHolderTable.parentNode + "table");
    deleteMap();
  }

  afterCombatButtons.forEach(function(button) {
    button.style.display = "none";
  });

    if (areaId == 4 & introShown == false) {
    intro.style.display = "block";
    introShown = true;
  }
  else{
    intro.style.display = "none";
  }

  titleNC.innerHTML = "";
  titleNC.style.display = "none";
  runText.innerHTML = "";
  runText.style.display = "none";
  initText.innerHTML = "";
  initText.style.display = "none";
  characterAll.style.display = "none";
  inventoryAll.style.display = "none";
  monsterStats.style.display = "none";
  game.style.display = "flex";
  text.innerHTML = maps[areaId].text;
  if (areaId != 0 && areaId != 1) {
    monsterName2.innerHTML = "";
    monsterType.innerHTML = "";
    monsterHealth2.innerHTML = "";
    monsterAc.innerHTML = "";
    monsterItems.innerHTML = "";
    monsterYen.innerHTML = "";
  }
  if (curMonActive == true && areaId != 0 && areaId != 1) {
    curMon.style.display = "none";
    curMonActive = false;
  }
  if (areaId != 0 && areaId != 1) {
    for (let i = 0; i <= maps.length - 1; i++) {
      // eval("m" + i).style.display = "none";
    }

    // eval("m" + areaId).style.display = "block";
  }

  if (areaId == 4 && fightOn == true) {
    runText.style.display = "block";
    runText.innerHTML += "You run away like a quick brown fox.";
    // createMap(currentArea);
    // mapImage.style.display = "block";
  }

 
  if (inventoryShown == true) {
    // mapImage.style.display = "block";
    // createMap(currentArea);
    inventoryShown = false;
  }

   fightOn = false;
  if (areaId != 0 && areaId != 1 && areaId != 2) {
            currentArea = areaId;
    createMap(currentArea);
    console.log(currentArea + "currAr");
  }
}





  
  function continueBU(){
    health = 10;
  healthText.innerHTML = health;
    update(4);
    afterText.style.display = "none";
  }

  afterText.onclick = continueBU;



  // items[i].amount --;
  // if(oldId != null){
  //   items[oldId].amount ++;
  // }
  
  // oldId = i;




function calcEncounterSelection(locationId) {
  locationId = currentArea;
  if (Math.random() <= maps[locationId].aProb) {
    return maps[locationId].a;
  }
  else if (Math.random() <= maps[locationId].bProb) {
    return maps[locationId].b;
  }
  else if (Math.random() <= maps[locationId].cProb) {
    return maps[locationId].c;
  }
  else {
    return maps[locationId].d;
  }
}


function loadFight(locationId){
  if(turns > 0){
  turns --;
  turnsText.innerText = turns;
  curMonActive = false;
  // areaSelection = monsterDist[locationId];
  // fighting = Math.floor(Math.random() * 4);
console.log(currentArea + "currentArea");
  // fighting = 3;
fighting = calcEncounterSelection(locationId);

  console.log(fighting + "fight");
  if((monsters[fighting].type == "nc")){
        battleMode();
    combatButtons.forEach(function(button) {
      button.style.display = "none";
    });
    afterCombatButtons.forEach(function(button) {
      button.style.display = "block";
    });
    button7.onclick = loadFight;
  button8.onclick = function() {
    update(4); // Call update function with areaId 4
}
    titleNC.style.display = "block";
   titleNC.innerHTML = monsters[fighting].name;
   text.innerHTML = monsters[fighting].text;
   console.log(fighting + "nc");
   itemDrop();
   text.innerHTML += "<br>";
   statDrop();
   values();
  }
  else{
    battleMode();
    fightOn = true;
    console.log(fightOn + "on");
  monsterHealth = monsters[fighting].health;
  console.log(monsterHealth + " health when loaded");
  monsterStats.style.display = "block";
  monsterName.innerHTML = monsters[fighting].name;
  monsterHealthText.innerHTML = monsterHealth;
  monsterName2.innerHTML = monsters[fighting].name;
  monsterType.innerHTML = monsters[fighting].type;
  monsterHealth2.innerHTML = monsters[fighting].health;
  monsterAc.innerHTML = monsters[fighting].ac;
  var drop1 = monsters[fighting].drop1;
  var drop2 = monsters[fighting].drop2;
  var drop3 = monsters[fighting].drop3;
  var drop4 = monsters[fighting].drop4;
  var drop5 = monsters[fighting].drop5;
  if (monsters[fighting].drop1 == null){
    monsterItems.innerHTML = "none";}
    else if(drop1 != null && drop2 == null){
      monsterItems.innerHTML = drop1;
    }
      else if(drop1 != null && drop2 != null && drop3 == null){
       monsterItems.innerHTML = drop1 + ", " + drop2;
      }
    
    else if(drop1 != null && drop2 != null && drop3!= null && drop4 == null){
     monsterItems.innerHTML = drop1 + ", " + drop2 + ", " + drop3;
    }
  
  else if(drop1 != null && drop2 != null && drop3!= null && drop4 != null && drop5 == null){
   monsterItems.innerHTML = drop1 + ", " + drop2 + ", " + drop3 + ", " + drop4;
  }

else if(drop1 != null && drop2 != null && drop3!= null && drop4 != null && drop5 != null){
  monsterItems.innerHTML = drop1 + ", " + drop2 + ", " + drop3 + ", " + drop4 + ", " + drop5;
}
  monsterYen.innerHTML = monsters[fighting].yen;
  rollInitiative();
}
  loadImage();
  // dreamget = 0;
  }
else{
  text.innerText = "\n\nYou're out of turns! Come back tomorrow.";
}
}



function loadImage(ex){
  // enemy.style.display = "none";
  //  if (areaId != 0 && areaId != 1){
    for (let i = 0; i <= monsters.length - 1; i++){
      eval("e" + i).style.display = "none";
    }
    for (let i = 0; i <= maps.length - 1; i++){
      eval("m" + i).style.display = "none";
    }
  // }
  console.log(monsters[fighting].id);
  curmon_helper = monsters[fighting].id;
  // console.log(typeof(curmon_helper));
  curMon = eval(curmon_helper);
  // String(curmon).style.display = "block";
//  for(let i=0; i <= monsters.length - 1; i++){
//   // console.log(curMon);
//   if(monsters[i].id != monsters[fighting].id){
//   eval(monsters[i].id).style.display = "none";
//   }
//  }
//  e0.style.display = "block";
  curMon.style.display = "block";
  curMonActive = true;
  
  // mit for ausblenden !!!!!!!!!!                            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
}


function defeatMonster(){
  combatButtons.forEach(function(button) {
    button.style.display = "none";
  });
  afterCombatButtons.forEach(function(button) {
    button.style.display = "block";
  });
  button7.onclick = loadFight;
  button8.onclick = function() {
    update(4); // Call update function with areaId 4
}
  var monsterRoll = dXRoll(20);
  if(monsterHealth >= 1){
  if(dXRoll(20) + monsters[fighting].atk >= ac){
  text.innerHTML += "You are hit for " + monsters[fighting].dam + " damage!<br><br>";
  console.log("monster attack " + monsterRoll);
  }
}
  // text.innerHTML += " You hit the " + monsters[fighting].name + " for " + damage + " damage.<br><br>";
  text.innerHTML += "You defeat the " + monsters[fighting].name + "! <br><br>You find " + monsters[fighting].yen + " Yen.";
  yen += monsters[fighting].yen;
  yenText.innerHTML = yen;
    itemDrop();
    text.innerHTML += "<br>";
    statDrop();
//     maxHealth = 10 + Math.floor(con * 2);
//  maxHealthText.innerHTML = maxHealth;
values();

//   console.log(items[0]["amount"] + "in my inv");
// console.log(items[0].amount + "fists");
  // { id: "i0", name: 'fist', amount: 0, category: "equipment", type: "weapon", attackBonus: 0, damageType: "bludgeoning", damageBonus: 0, handedness: "one"},
  // const monsters drop1: "fist",     dropprob1: 0.5,
  initRound = true;
  initRounder.innerHTML += " true";
  monsterInitWon = false;
  fightOn = false;
  }

  



  function stripId(id) {
    id = id.replace(/[a-z]/g, ""); // Using the global flag to replace all occurrences
    return id;
  }


  function itemDrop() {
    var dropped = false; // Variable to track if any item is dropped

    // Check drop chance for each item independently
    if (Math.random() <= monsters[fighting].dropprob1) {
        var dropItem = items[stripId(monsters[fighting].drop1)];
        items[stripId(monsters[fighting].drop1)]["amount"]++;
        text.innerHTML += "<br><br>You find 1 " + monsters[fighting].drop1name + ". ";
        text.appendChild(createItemImage(dropItem));
        dropped = true; // Set dropped to true
    }



    

    if (Math.random() <= monsters[fighting].dropprob2) {
        var dropItem = items[stripId(monsters[fighting].drop2)];
        items[stripId(monsters[fighting].drop2)]["amount"]++;
        text.innerHTML += "<br><br>You find 1 " + monsters[fighting].drop2name + ". ";
        text.appendChild(createItemImage(dropItem));
        dropped = true; // Set dropped to true
    }

    if (Math.random() <= monsters[fighting].dropprob3) {
        var dropItem = items[stripId(monsters[fighting].drop3)];
        items[stripId(monsters[fighting].drop3)]["amount"]++;
        text.innerHTML += "<br><br>You find 1 " + monsters[fighting].drop3name + ". ";
        text.appendChild(createItemImage(dropItem));
        dropped = true; // Set dropped to true
    }

    if (Math.random() <= monsters[fighting].dropprob4) {
        var dropItem = items[stripId(monsters[fighting].drop4)];
        items[stripId(monsters[fighting].drop4)]["amount"]++;
        text.innerHTML += "<br><br>You find 1 " + monsters[fighting].drop4name + ". ";
        text.appendChild(createItemImage(dropItem));
        dropped = true; // Set dropped to true
    }

    if (Math.random() <= monsters[fighting].dropprob5) {
        var dropItem = items[stripId(monsters[fighting].drop5)];
        items[stripId(monsters[fighting].drop5)]["amount"]++;
        text.innerHTML += "<br><br>You find 1 " + monsters[fighting].drop5name + ". ";
        text.appendChild(createItemImage(dropItem));
        dropped = true; // Set dropped to true
    }

    // Return the image element if an item is dropped, null otherwise
    return dropped ? text.lastElementChild : null;
}

function createItemImage(item) {
  var itemImg = document.createElement("img");
  itemImg.src = "Items/" + item.id + ".jpg"; // Set the src attribute dynamically based on the item id
  
  itemImg.width = "40";
  itemImg.height = "40";
  itemImg.className = "item-image";
  // itemImg.display = "block";
  console.log("Item ID:", item.id);
  return itemImg;
}

let thresholds = [2, 4, 7, 11, 16, 22, 29, 37, 46, 56, 67, 79, 92, 106, 121, 137, 154, 172];

let primaryModifier = {a: 0, aProb: 0.1, b: 1, bProb: 0.2, c: 2, cProb: 0.4, d: 3, dProb: 0.2, e: 4 , eProb: 0.1};
let secondaryModifier = {a: 0, aProb: 0.2, b: 1, bProb: 0.3, c: 2, cProb: 0.2, d: 3, dProb: 0.2, e: 4 , eProb: 0.1};
let tertiaryModifier = {a: 0, aProb: 0.4, b: 1, bProb: 0.3, c: 2, cProb: 0.1, d: 3, dProb: 0.1, e: 4 , eProb: 0.1};


function selectRandomEntry(obj) {
  let randomNumber = Math.random();
  let cumulativeProbability = 0;

  for (let key in obj) {
      if (key.includes('Prob')) {
          cumulativeProbability += obj[key];
          if (randomNumber <= cumulativeProbability) {
              return obj[key.replace('Prob', '')];
          }
      }
  }

  // If no entry is selected, return null or handle it based on your requirement
  return null;
}

// character.onclick = function() {
//   let randomEntry = selectRandomEntry(primaryModifier);
//   console.log("Random Entry:", randomEntry);
// };


function statDrop(){
  let classPrimaryAttribute = monsters[fighting].level/10;
  let classSecondaryAttribute = monsters[fighting].level/15;
  let classTertiaryAttribute = monsters[fighting].level/20;
  let randomStr = selectRandomEntry(primaryModifier);
  let randomDex = selectRandomEntry(secondaryModifier);
  let randomCon = selectRandomEntry(secondaryModifier);
  let randomInt = selectRandomEntry(tertiaryModifier);
  let randomWis = selectRandomEntry(tertiaryModifier);
  let randomCha = selectRandomEntry(tertiaryModifier);
  classStrRounder = Number((classPrimaryAttribute * randomStr).toFixed(2));
    str += classStrRounder;
    strText.innerHTML = str.toFixed(2);
    if(randomStr != 0){
    text.innerHTML += "<br>You gain " + classStrRounder + " STR.";
    }
    classDexRounder = Number((classSecondaryAttribute * randomDex).toFixed(2));
    classConRounder = Number((classSecondaryAttribute * randomCon).toFixed(2));
    dex += classDexRounder;
    dexText.innerHTML = dex.toFixed(2);
    if(randomDex != 0){
    text.innerHTML += "<br>You gain " + classDexRounder + " DEX.";
    }
    con += classConRounder;
    conText.innerHTML = con.toFixed(2);
    if(randomCon != 0){
    text.innerHTML += "<br>You gain " + classConRounder + " CON.";
    }
    classIntRounder = Number((classTertiaryAttribute * randomInt).toFixed(2));
    classWisRounder = Number((classTertiaryAttribute * randomWis).toFixed(2));
    classChaRounder = Number((classTertiaryAttribute * randomCha).toFixed(2));
    int += classIntRounder;
    intText.innerHTML = int.toFixed(2);
    if(randomInt != 0){
    text.innerHTML += "<br>You gain " + classIntRounder + " INT.";
    }
    wis += classWisRounder;
    wisText.innerHTML = wis.toFixed(2);
    if(randomWis != 0){
    text.innerHTML += "<br>You gain " + classWisRounder + " WIS.";
    }
    
    cha += classChaRounder;
    chaText.innerHTML = cha.toFixed(2);
    if(randomCha != 0){
    text.innerHTML += "<br>You gain " + classChaRounder + " CHA.";
    }
if(str >= thresholds[0]){
  thresholds.shift();    
  level ++;
      levelText.innerHTML = level;
      text.innerHTML += "<br><br>You have gained a level and are now level " + level +"!";
      levelBonus ++;
      // maxHealth +=2;
      // maxHealthText.innerText = maxHealth;
      updateHealth(2);
      // maxMana +=2;
      // maxManaText.innerText = maxMana;
      updateMana(2);
      // maxStamina +=2;
      // maxStaminaText.innerText = maxStamina;
      updateStamina(2);
      //       console.log(str + "str");
      // console.log(thresholds + "bp");
    }
}

let conIncrease = 2;
let oldCon;


function values(){
  
  oldCon = Math.floor(con - 1);
  
    if (Math.floor(oldCon) >= 1 && Math.floor(con) > Math.floor(oldCon)) {
    conIncrease = Math.floor(con) * 2;
    oldCon--;
  }

  
 maxHealth = 10 + conIncrease + (levelBonus * 2) + healthBonus;
 maxHealthText.innerHTML = maxHealth;
  
  maxMana = 10 + Math.floor(int + wis + cha) + (levelBonus * 2) + manaBonus;
  maxManaText.innerHTML = maxMana;
  
  maxStamina = 10 + conIncrease + (levelBonus * 2) + staminaBonus;
  maxStaminaText.innerHTML = maxStamina;
  }

// when health etc hit threshold 1 2 3, health +


function beatenUp(){
if(health < 1){
  
  afterCombatButtons.forEach(function(button) {
    button.style.display = "none";
  });
  
  combatButtons.forEach(function(button) {
    button.style.display = "none";
  });
  var monsterRoll = dXRoll(20);
  console.log(monsterRoll + "rolled");
    //  health -= monsters[fighting].dam;
    // healthText.innerHTML = health;
    // text.innerHTML += "You are hit for " + monsters[fighting].dam + " damage! ";
   text.innerHTML += " You have been defeated by the " + monsters[fighting].name + "!<br><br> Your consciousness fades... but it's all right – you wake up in your bed, sore but alive. Some of the yen you had in your pocket are missing, however. Curse you, Pokemon logic!";
  yen -= dXRoll(20);
  yenText.innerHTML = yen;
  initRound = true;
  initRounder.innerHTML += " true";
  monsterInitWon = false;
  afterText.style.display = "block";
  fightOn = false;
  return;
}

}


function rollInitiative() {
  let playerInitX = dXRoll(20) + initBonus;
  let monsterInitX = dXRoll(20) + monsters[fighting].init;

  if (initRound == true) {
    if (playerInitX >= monsterInitX) {
      playerInit.innerHTML = playerInitX;
      monsterInit.innerHTML = monsterInitX;
      initText.style.display = "block";
      initText.innerHTML += "You are fighting " + monsters[fighting].indef + " " + monsters[fighting].name +".<br><br>" + "You take the initiative.<br>";
      monsterInitWon = false;
      playerInitX = 0;
      monsterInitX = 0;
    }
    else {
      playerInit.innerHTML = playerInitX;
      monsterInit.innerHTML = monsterInitX;
      initText.style.display = "block";
      initText.innerHTML += "You are fighting " + monsters[fighting].indef + " " + monsters[fighting].name +".<br><br>" + "The " + monsters[fighting].name + " takes the initiative.<br>";
      monsterInitWon = true;
      playerInitX = 0;
      monsterInitX = 0;
    }
  }
 
}


let isUsingSkill = false; // Flag to indicate if a skill is being used

// Add event listener to the "Use skill" button
const buttonUseSkill = document.querySelector('#button1'); // Assuming you've already defined this button
buttonUseSkill.addEventListener('click', () => {
    isUsingSkill = true; // Set the flag to true when the "Use skill" button is clicked
    selectedAction = "useSkill"; // Set selectedAction accordingly
    fight(); // Call the fight function when the button is clicked
});

const buttonUseSkill2 = document.querySelector('#button2'); // Assuming you've already defined this button
buttonUseSkill2.addEventListener('click', () => {
    isUsingSkill = true; // Set the flag to true when the "Use skill" button is clicked
    selectedAction = "useSkill"; // Set selectedAction accordingly
    fight(); // Call the fight function when the button is clicked
});

function playerAttack() {
  if (selectedAction == "attack") {
      attackRoll = calcAtk();
      if (attackRoll >= monsters[fighting].ac) {
          damage = calcDam();
          console.log(monsterHealth + "health before");
          console.log(damage + " damage logged in playerAttack");
          monsterHealth -= damage;
          console.log(monsterHealth + "health after");
          monsterHealthText.innerHTML = monsterHealth;
          text.innerHTML += " You hit the " + monsters[fighting].name + " for " + damage + " damage!<br><br>";
      } else {
          text.innerHTML += " You miss the " + monsters[fighting].name + ".<br><br>";
      }
  } else if (isUsingSkill) {
      var selectedSkillId1 = selectSkill1.value;
      var selectedSkillId2 = selectSkill2.value;
      // text.innerHTML = "<br><br>";
      if (monsterHealth >= 1) { // Check if monster is still alive before using the second skill
          useCombatSkill(selectedSkillId1);
          console.log("first skill used"); // Logging message changed
          if (monsterHealth >= 1) { // Check if monster is still alive after using the first skill
              useCombatSkill(selectedSkillId2); // Use the second skill
              console.log("second skill used"); // Logging message changed
          }
      }
      isUsingSkill = false; // Reset the flag
  }
  isUsingSkill = false; // Reset the flag
  console.log(monsterHealth + "monster health");
}





// Filter skills that are learned and have type "combat" or "hybrid"
const filteredSkills = skills.filter(skill => skill.learned && (skill.type === "combat" || skill.type === "hybrid"));

// Define a function to use skills in combat
function useCombatSkill(selectedSkillId1, selectedSkillId2) {
  const selectedSkill1 = skills.find(skill => skill.id === selectedSkillId1);
  const selectedSkill2 = skills.find(skill => skill.id === selectedSkillId2);

  // Use skill 1
  if (selectedSkill1) {
      switch (selectedSkill1.effect1) {
          case 'damage':
            var cost1 = selectedSkill1.cost1;
            if(selectedSkill1.costType1 == "mana"){
            mana -= cost1;
            manaText.innerHTML = mana;
            }
            else if(selectedSkill1.costType1 == "stamina"){
              stamina -= cost1;
              staminaText.innerHTML = stamina;
            }
              const damage1 = selectedSkill1.effect1Variable;
              monsterHealth -= damage1;
              monsterHealthText.innerHTML = monsterHealth;
              text.innerHTML += `You use ${selectedSkill1.name} on the ${monsters[fighting].name} for ${damage1} damage!<br><br>`;
              console.log("skill 1 used");
              break;
          // You can add cases for other effects here
          default:
              console.log('Unknown effect');
      }
  } else {
      console.log('No skill selected or selected skill is invalid.');
  }

  // Check if monster is still alive and if the second skill is available
  if (monsterHealth >= 1 && selectedSkill2) {
      // Use skill 2
      switch (selectedSkill2.effect1) {
          case 'damage':
            var cost1 = selectedSkill1.cost1;
            if(selectedSkill1.costType1 == "mana"){
            mana -= cost1;
            manaText.innerHTML = mana;
            }
            else if(selectedSkill1.costType1 == "stamina"){
              stamina -= cost1;
              staminaText.innerHTML = stamina;
            }
              const damage2 = selectedSkill2.effect1Variable;
              monsterHealth -= damage2;
              monsterHealthText.innerHTML = monsterHealth;
              text.innerHTML += `You use ${selectedSkill2.name} on the ${monsters[fighting].name} for ${damage2} damage!<br><br>`;
              console.log("skill 2 used");
              break;
          // You can add cases for other effects here
          default:
              console.log('Unknown effect');
      }
  }
}






// Select the <select> element for skill 1
const selectSkill1 = document.querySelector('#selectSkill1');

// Populate the <select> element with the names of filtered skills
filteredSkills.forEach(skill => {
  const option = document.createElement('option');
  option.value = skill.id; // Set the value attribute to skill id if needed
  option.textContent = skill.name;
  selectSkill1.appendChild(option);
});

const selectSkill2 = document.querySelector('#selectSkill2');

// Populate the <select> element with the names of filtered skills
filteredSkills.forEach(skill => {
  const option = document.createElement('option');
  option.value = skill.id; // Set the value attribute to skill id if needed
  option.textContent = skill.name;
  selectSkill2.appendChild(option);
});


// Add event listener to the "Attack" button
const buttonAttack = document.querySelector('#button6'); // Assuming you've already defined this button
buttonAttack.addEventListener('click', () => {
    selectedAction = "attack";
    // fight();
});

// // Add event listener to button1 to use the selected skill in combat
// button1.addEventListener('click', () => {
  
  
// });



function monsterAttack(){
  var monsterRoll = dXRoll(20);
  if (monsterRoll + monsters[fighting].atk >= ac) {
    health -= monsters[fighting].dam;
    healthText.innerHTML = health;
    text.innerHTML += "You are hit by the " + monsters[fighting].name + " for " + monsters[fighting].dam + " damage! ";
  }
  // monster miss
  else {
    text.innerHTML += " The " + monsters[fighting].name + " misses.";
  }
  console.log("monster attack " + monsterRoll);
}

// monsterInitWon = true;

function fight() {
  //  player wins initiative
  initText.innerHTML = "";
  initText.style.display = "none";
  text.innerHTML = "";
  // player attack hit
  console.log(initRound);
  console.log(monsterInitWon);
  if (monsterInitWon == false) {
    playerAttack();
    if (monsterHealth < 1) {
      defeatMonster();
    }
    if (monsterHealth >= 1) {
      monsterAttack();
      beatenUp();
    }
  }
  else {
      monsterAttack();
      beatenUp();
      if (health >= 1) {
        playerAttack();
        if (monsterHealth < 1) {
          defeatMonster();
        }
      }
    }
  
}



// placeholder for button functions
// button1.onclick = sleep;
// button2.onclick = sleep;
button3.onclick = sleep;
button4.onclick = sleep;
button5.onclick = function() {
  update(4);
    combatButtons.forEach(function(button) {
      button.style.display = "none";
    })
}
button6.onclick = fight;


  function battleMode(areaId) {
text.innerHTML = "";

afterCombatButtons.forEach(function(button) {
  button.style.display = "none";
});

combatButtons.forEach(function(button) {
  button.style.display = "block";
});

  // if (mapHolderTable !== null){
    mapHolderTable = document.querySelector('#mapHolder > table');
    deleteMap();
  // }

    if (areaId == 4 & introShown == false) {
    intro.style.display = "block";
    introShown = true;
  }
  else{
    intro.style.display = "none";
  }

  titleNC.innerHTML = "";
  titleNC.style.display = "none";
  runText.innerHTML = "";
  runText.style.display = "none";
  initText.innerHTML = "";
  initText.style.display = "none";
  characterAll.style.display = "none";
  inventoryAll.style.display = "none";
  monsterStats.style.display = "none";
  // button1.innerHTML = "use item";
  // button2.innerHTML = "use item 2";
  // button3.innerHTML = "use skill";
  // button4.innerHTML = "use skill 2";
  // button5.innerHTML = "run";
  // button6.innerHTML = "attack";
  
  
  if (areaId != 0 && areaId != 1) {
    monsterName2.innerHTML = "";
    monsterType.innerHTML = "";
    monsterHealth2.innerHTML = "";
    monsterAc.innerHTML = "";
    monsterItems.innerHTML = "";
    monsterYen.innerHTML = "";
  }
  if (curMonActive == true && areaId != 0 && areaId != 1) {
    curMon.style.display = "none";
    curMonActive = false;
  }
  if (areaId != 0 && areaId != 1) {
    for (let i = 0; i <= maps.length - 1; i++) {
    }
  }

  if (areaId == 4 && fightOn == true) {
    runText.style.display = "block";
    runText.innerHTML += "You run away like a quick brown fox.";

  }
 
  if (inventoryShown == true) {
    inventoryShown = false;
  }

   fightOn = false;
  // if (areaId != 0 && areaId != 1 && areaId != 2) {
  //           currentArea = areaId;
  //   createMap(currentArea);
  //   console.log(currentArea + "currAr");
  // }
}

function updateHealth(amount) {
  // Update health with the given amount
  health += amount;
  
  // Ensure health doesn't exceed maxHealth
  health = Math.min(health, maxHealth);
  
  // Update health display
  healthText.innerHTML = health;
}

function updateMana(amount) {
  // Update health with the given amount
  mana += amount;
  
  // Ensure health doesn't exceed maxHealth
  mana = Math.min(health, maxMana);
  
  // Update health display
  manaText.innerHTML = mana;
}

function updateStamina(amount) {
  // Update health with the given amount
  stamina += amount;
  
  // Ensure health doesn't exceed maxHealth
  stamina = Math.min(stamina, maxStamina);
  
  // Update health display
  staminaText.innerHTML = stamina;
}

function sleep() {
  titleNC.innerHTML = "";
  titleNC.style.display = "none";
  runText.innerHTML = "";
  runText.style.display = "none";
  intro.innerHTML = "";
  intro.style.display = "none";

  updateHealth(10);
  
  // Update the health display
  healthText.innerHTML = health;
  
  // Reset dreamget
  // dreamget = 0;
  turns --;
  turnsText.innerText = turns;

  // Display a message to the player
  text.innerHTML = "You nap for a little while to regain some of your strength (actually health).";
}

// function text0(){
//   dreamget = 1;
//   text.innerHTML = "You're already dreaming.";
// }

function description(i){
  text.innerHTML = descriptions[i];
  intro.style.display = "none";
  titleNC.innerHTML = "";
  titleNC.style.display = "none";
  runText.innerHTML = "";
  runText.style.display = "none";
  initText.innerHTML = "";
  initText.style.display = "none";
  characterAll.style.display = "none";
  inventoryAll.style.display = "none";
  monsterStats.style.display = "none";
}


function equipment(){
  
  }

function stuff(){
  
    }

function consumables(){
  
      }


function dXRoll(x) {
  var dieRoll = Math.floor(Math.random() * x + 1);
    console.log(dieRoll + "rolled");
  return dieRoll;
}

function dXRollPlayer(x) {
  var dieRoll = Math.floor(Math.random() * x + 1);
  d20Text.innerHTML = dieRoll;
  console.log(dieRoll + "attack rolled by player");
  return dieRoll;
}



// function cancelInv(){
//   ul.remove('li');
//   alert("hi");
// }



// ???
function showItem(i) {
  var item = items[i];
  var itemProperties = Object.keys(item);
  var itemDetails = "";

  itemProperties.forEach(function(property) {
      itemDetails += property + ": " + item[property] + "<br>";
  });

  itemValues.innerHTML = itemDetails;
}





  function createMap(currentArea) {
    var mapHolderTable = document.querySelector('#mapHolder table');
    if (mapHolderTable) {
        mapHolderTable.remove();
    }

    // var mapHolder = document.getElementById('mapHolder');

    // Create table element and set its attributes
    var table = document.createElement('table');
    table.setAttribute('style', 'display: grid; grid-template-columns: repeat(7, 1fr)');

    if (!mapCreated) { // Assuming mapCreated is a global variable
        var mapData = maps[currentArea];
        if (!mapData) {
            console.error("Map data not found for currentArea:", currentArea);
            return;
        }

        var mapArray = mapData.mapArray;

        // Ensure mapArray is an array
        if (!Array.isArray(mapArray)) {
            console.error("Invalid mapArray: not an array.", mapArray);
            return;
        }

        // console.log("Filtered mapArray:", mapArray);

        var mapFunctionsString = mapData.mapFunctionsArray;

        // console.log("mapFunctionsArray type:", typeof mapFunctionsString);
        // console.log("mapFunctionsArray content:", mapFunctionsString);

        // Extract function strings using regular expressions
        var regex = /\"[^"]*\"/g;
        var mapFunctionsArray = mapFunctionsString.match(regex);

        // console.log("Parsed mapFunctionsArray:", mapFunctionsArray);

        // Check if mapFunctionsArray is an array
        if (Array.isArray(mapFunctionsArray)) {
            for (let i = 0; i < mapArray.length; i++) {
                var img = document.createElement("img");
                var mapElement = mapArray[i];
                img.setAttribute("src", "Map_elements/el" + mapElement + ".jpg");
                img.setAttribute("style", "display: block; border: solid 1px black");
                img.setAttribute("id", "el" + i);
                img.setAttribute("class", "element");
                img.setAttribute("width", "125px");
                img.setAttribute("height", "125px");

                // Assign onclick event to the image element
                if (mapFunctionsArray[i]) {
                  var funcString = mapFunctionsArray[i].replace(/"/g, '');
                  // console.log("Function string:", funcString); // Log the function string
                  img.onclick = eval("(" + funcString + ")");
                }



                table.appendChild(img);
            }
            mapHolder.appendChild(table);
            console.log("Map table created");
        } else {
            console.error("Failed to parse mapFunctionsArray.");
        }
    }
    
}


  
  function deleteMap(){
    var elElements = document.querySelectorAll('[id^="el"]');
    
    // Iterate over the selected elements and remove each one
    elElements.forEach(function(element) {
      element.remove();
    });
  }

  //  TODO: still needed but why??? path is missing the m why?

  for (let i = 0; i <= maps.length - 1; i++){
    var mapCreator = document.getElementById('mapImage');
        let mapImg = document.createElement("img");
        mapImg.setAttribute("src", "Maps/" + (maps[i].id + ".jpg"));

        // mapImg.style.background="url('Maps/el1.jpg')"
        // bigImg.setAttribute("width", "300px");
        mapImg.setAttribute("id","m"+ i);
        mapImg.setAttribute ("class","maps");
        // mapImg.setAttribute ("style", "display: none")
    // imgCreator.setAttribute("src", "Enemies/" + "e0.jpg");
    mapCreator.appendChild(mapImg);
  }



  for (let i = 0; i <= monsters.length - 1; i++){
    var imgCreator = document.getElementById('bigImage');
        let bigImg = document.createElement("img");
        bigImg.setAttribute("src", "Monsters/" + (monsters[i].id + ".jpg"));
        bigImg.setAttribute("width", "300px");

                bigImg.setAttribute("id","e"+ i);
        bigImg.setAttribute("class","enemyPic");
  
    imgCreator.appendChild(bigImg);
  }
  // console.log(e0);
  

  function createInv() {
    var inv = document.getElementById('inventoryAll');
    inv.innerHTML = "";

    // create ul element and set its attributes.
    const ul = document.createElement('ul');
    ul.setAttribute('style', 'padding: 0; margin: 0; display: grid; list-style: none; grid-template-columns: repeat(5, 1fr)');
    ul.setAttribute('id', 'theList');

    for (let i = 0; i < items.length; i++) {
        if (items[i].amount !== 0) {
            const li = document.createElement('li'); // create li element.
            var img = document.createElement("img");
            img.setAttribute("src", "Items/" + items[i].id + ".jpg");
            img.setAttribute("width", "40px");
            img.setAttribute("height", "40px");
            img.setAttribute("class", "item");

            li.innerHTML = "<strong>" + items[i].name + "</strong>" + " (" + items[i].amount + ")" + "<br>";
            if (items[i].type === "weapon") {
                li.innerHTML += items[i].handedness + "-handed " + items[i].subtype + " ";
            }
            li.innerHTML += items[i].type; // assigning text to li using array value.
            li.setAttribute('style', 'display: block;'); // remove the bullets.
            li.setAttribute('class', 'item');

            // Add onclick event to show item details
            li.setAttribute("onclick", "showItem(" + i + ")");

            li.appendChild(img);

            // Add equip text for equipment items
            if (items[i].category === "equipment") {
                li.innerHTML += "<br>" + `<span onclick="equipItem(${i})">equip</span>`;
            }

            ul.appendChild(li); // append li to ul.
        }
    }
    inv.appendChild(ul); // add ul to the container.
}


function showCharacter(){
  console.log(fightOn);
  
  if(fightOn != true){
  for (let i = 0; i <= monsters.length - 1; i++) {
    let monsterImg = document.getElementById("e" + i); // Assuming monster images have IDs like "e0", "e1", etc.
    if (monsterImg) {
        monsterImg.style.display = "none"; // Hide the monster picture
    }
}
  }
  intro.style.display = "none";
text.innerHTML = "";
runText.innerHTML = "";
  inventoryShown = true;
  titleNC.innerHTML = "";
  titleNC.style.display = "none";
  inventoryAll.style.display = "none";
  game.style.display = "none";
  characterAll.style.display = "block";
  playerNameC.innerText = playerName;
  playerSexC.innerText = playerSex;
playerClassC.innerText = playerClass;
levelTextC.innerHTML = level;
strTextC.innerHTML = str;
dexTextC.innerHTML = dex;
conTextC.innerHTML = con;
intTextC.innerHTML = int;
wisTextC.innerHTML = wis;
chaTextC.innerHTML = cha;
healthTextC.innerHTML = health;
maxHealthTextC.innerHTML = maxHealth;
manaTextC.innerHTML = mana;
maxManaTextC.innerHTML = maxMana;
staminaTextC.innerHTML = stamina;
maxStaminaTextC.innerHTML = maxStamina;

skillsC.innerHTML = "";

const learnedSkills = skills.filter(skill => skill.learned);

   // Construct the HTML for the list of learned skills
   const skillsHTML = learnedSkills.map(skill => {
    let skillHTML = `<li>${skill.name}`;
    if (skill.name === "Colour vision") {
        // Append a button for toggling on/off
        skillHTML += ` <button class="toggle-button" onclick="toggleColourVision(this)">Off</button>`;
    }
    skillHTML += `</li>`;
    return skillHTML;
}).join("");

// Set the HTML content of the skillsC element
skillsC.innerHTML = `<ul id="characterSkillsList">${skillsHTML}</ul>`;

if (fightOn == true) {
    initText.style.display = "none";
    text.innerHTML = "You are in the middle of a fight!"
}
deleteMap();
}

let colourVisionActive = false;

// Function to toggle the state of "colour vision"
function toggleColourVision(button) {
    colourVisionActive = !colourVisionActive;
    const text = colourVisionActive ? "On" : "Off";
    button.textContent = text;

    // Find all enemy pictures
    const enemyPics = document.querySelectorAll('.enemyPic');
    enemyPics.forEach((pic, index) => {
        if (colourVisionActive) {
            // If "colour vision" is turned on, change the image source to include "_c.jpg"
            pic.setAttribute("src", "Monsters/" + (monsters[index].id + "_c.jpg"));
        } else {
            // If "colour vision" is turned off, revert the image source to its original state
            pic.setAttribute("src", "Monsters/" + (monsters[index].id + ".jpg"));
        }
    });
}


function showInventory(){
  console.log(fightOn);
  
  if(fightOn != true){
  for (let i = 0; i <= monsters.length - 1; i++) {
    let monsterImg = document.getElementById("e" + i); // Assuming monster images have IDs like "e0", "e1", etc.
    if (monsterImg) {
        monsterImg.style.display = "none"; // Hide the monster picture
    }
}
  }
  intro.style.display = "none";
text.innerHTML = "";
runText.innerHTML = "";
  inventoryShown = true;
  titleNC.innerHTML = "";
  titleNC.style.display = "none";
  characterAll.style.display = "none";
  monsterStats.style.display = "none";
  game.style.display = "none";
  
  for (let i = 0; i <= monsters.length - 1; i++){
    if(fightOn == false){
    // eval("e"+ i).style.display = "none";
    inventoryAll.style.display = "block";
  // mapImage.style.display = "none";
  createInv();
    }
    }
    if(fightOn == true){
      initText.style.display = "none";
    text.innerHTML = "You are in the middle of a fight!"
  }
  deleteMap();
}


// -- currently scrapped --

// dualWieldingToggle.addEventListener("click", function() {
//   dualWielding = !dualWielding; // Toggle the state
//   // Update the text content of the toggle button
//   this.textContent = dualWielding ? "On" : "Off";
// });


console.log(typeof(ac) + "before");
console.log(equippedTorsoAcBonus + "before");
console.log(typeof(equippedTorsoAcBonus) + "before");

function equipItem(i) {
  // Store the selected item's details
  const selectedItem = items[i];

  // Check if the player has the item in their inventory
  if (selectedItem.amount < 1) {
    console.error("Cannot equip item - not enough available.");
    return;
  }

  // Define a function to append "unequip" text
  function appendUnequipText(itemName) {
    return itemName + " (unequip)";
  }

  // Get equipment properties based on the selected item
  setEquipmentProperties(selectedItem);


  // Update the text content for the corresponding slot with the selected item's name (appending "unequip" text)
  switch (selectedItem.category) {
    case "equipment":
      switch (selectedItem.type) {
        case "weapon":
          // Handle weapon types
          if (selectedItem.handedness === "two") {
            if (!dualWielding) {
              // Unequip any existing one-handed weapon in both hands
              if (equippedWeapon1 !== null && equippedWeapon1 !== "fist") {
                unequipItem("equippedWeapon1", equippedWeapon1);
              }
              if (equippedWeapon2 !== null && equippedWeapon2 !== "fist") {
                unequipItem("equippedWeapon2", equippedWeapon2);
              }
            } else {
              // If dual-wielding is on and a two-handed weapon is in the right hand,
              // replace it with the one-handed weapon and clear the left hand
              if (equippedWeapon1 === "fist" && equippedWeapon2 === "occupied") {
                equippedWeapon1 = selectedItem.id;
                equippedWeapon1Text.innerHTML = appendUnequipText(selectedItem.name);
                equippedWeapon1Text.onclick = function() { unequipItem("equippedWeapon1", selectedItem.id); };
                equippedWeapon2Text.innerHTML = "";
                equippedWeapon2 = "occupied";
              } else {
                // Unequip any existing one-handed weapon in the left hand when replaced by another one-handed weapon
                if (dualWielding && equippedWeapon2 !== null && equippedWeapon2 !== "fist" && selectedItem.subtype === items.find(item => item.id === equippedWeapon2).subtype) {
                  unequipItem("equippedWeapon2", equippedWeapon2);
                }
              }
            }
            equippedWeapon1 = selectedItem.id;
            equippedWeapon1Text.innerHTML = appendUnequipText(selectedItem.name);
            equippedWeapon1Text.onclick = function() { unequipItem("equippedWeapon1", selectedItem.id); };
            equippedWeapon2Text.innerHTML = "occupied";
            equippedWeapon2Text.onclick = null;
          } else if (selectedItem.handedness === "one") {
            if (!dualWielding) {
              // Unequip any existing one-handed weapon in both hands
              if (equippedWeapon1 !== null && equippedWeapon1 !== "fist") {
                unequipItem("equippedWeapon1", equippedWeapon1);
              }
              // if (equippedWeapon2 !== null && equippedShield2Type !== "shield") {
              //   unequipItem("equippedWeapon2", equippedWeapon2);
              // }
              equippedWeapon1 = selectedItem.id;
              equippedWeapon1Text.innerHTML = appendUnequipText(selectedItem.name);
              equippedWeapon1Text.onclick = function() { unequipItem("equippedWeapon1", selectedItem.id); };
            } else {
              if (equippedWeapon2 !== "occupied" && equippedWeapon1 === "fist") {
                equippedWeapon1 = selectedItem.id;
                equippedWeapon1Text.innerHTML = appendUnequipText(selectedItem.name);
                equippedWeapon1Text.onclick = function() { unequipItem("equippedWeapon1", selectedItem.id); };
              } else {
                equippedWeapon2 = selectedItem.id;
                equippedWeapon2Text.innerHTML = appendUnequipText(selectedItem.name);
                equippedWeapon2Text.onclick = function() { unequipItem("equippedWeapon2", selectedItem.id); };
              }
            }
          }
          
          break;
        
        case "shield":
          // Unequip any two-handed weapon
          console.log(equippedWeapon1 + "before");
          if (equippedWeapon1 !== null && equippedWeapon1Handedness === "two") {
            unequipItem("equippedWeapon1", equippedWeapon1);
          }
          console.log(equippedWeapon1 + "after");
          // if (equippedWeapon1 !== null && equippedWeapon2 === "occupied") {
          //   unequipItem("equippedWeapon2", equippedWeapon2);
          // }
          // Equip the shield
          equippedWeapon2 = selectedItem.id;
          equippedWeapon2Text.innerHTML = appendUnequipText(selectedItem.name);
          equippedWeapon2Text.onclick = function() { unequipItem("equippedWeapon2", selectedItem.id); };
          break;
          case "helmet":
            if (equippedHelmet !== null && equippedHelmet !== undefined) {
              unequipItem("equippedHelmet", equippedHelmet);
            }
            equippedHelmet = selectedItem.id;
            equippedHelmetText.innerHTML = appendUnequipText(selectedItem.name);
            equippedHelmetText.onclick = function() { unequipItem("equippedHelmet", equippedHelmet); };
            break;
        case "torso":
          if (equippedTorso !== null && equippedTorso !== undefined) {
            unequipItem("equippedTorso", equippedTorso);
          }
          equippedTorso = selectedItem.id;
          equippedTorsoText.innerHTML = appendUnequipText(selectedItem.name);
          equippedTorsoText.onclick = function() { unequipItem("equippedTorso", equippedTorso); };
          break;
        case "back":
          if (equippedBack !== null && equippedBack !== undefined) {
            unequipItem("equippedBack", equippedBack);
          }
          equippedBack = selectedItem.id;
          equippedBackText.innerHTML = appendUnequipText(selectedItem.name);
          equippedBackText.onclick = function() { unequipItem("equippedBack", equippedBack); };
          break;
        case "pants":
          if (equippedPants !== null && equippedPants !== undefined) {
            unequipItem("equippedPants", equippedPants);
          }
          equippedPants = selectedItem.id;
          equippedPantsText.innerHTML = appendUnequipText(selectedItem.name);
          equippedPantsText.onclick = function() { unequipItem("equippedPants", equippedPants); };
          break;
          case "accessory":
            // Handle accessory types
            let equippedSlotFound = false;
            for (let j = 1; j <= 4; j++) {
              const equippedAccessoryText = document.querySelector("#equippedAccessory" + j);
              if (equippedAccessoryText.innerHTML === "") {
                // If the slot is empty, equip the item
                equippedAccessoryText.innerHTML = appendUnequipText(selectedItem.name);
                equippedAccessoryText.onclick = function() { unequipItem("equippedAccessory" + j, selectedItem.id); };
                // Update the equippedAccessory variable for the slot
                window["equippedAccessory" + j] = selectedItem.id;
                equippedSlotFound = true;
                break;
              }
            }
            if (!equippedSlotFound) {
              console.error("Cannot equip accessory - all slots are occupied.");
              break;
            }
          }
    default:
      // console.error("Unknown item category:", selectedItem.category);
      break;
  }

  // Decrease the amount of the equipped item
  selectedItem.amount--;

  // Update the inventory display
  showInventory();
  setEquipmentProperties(selectedItem);
  equipStatUpdate();
  values();
  // Log the equipped item's details
  console.log(selectedItem.name + " equipped");
  console.log(typeof(ac) + "after");
  console.log(equippedTorsoAcBonus  + "after");
}



function setEquipmentProperties(selectedItem) {
  
  switch (selectedItem.type) {
    case "weapon":
       equippedWeapon1Id = selectedItem.id;
 equippedWeapon1Name = selectedItem.name;
 equippedWeapon1Amount = selectedItem.amount;
 equippedWeapon1Category = selectedItem.category;
 equippedWeapon1Type = selectedItem.type;
 equippedWeapon1Subtype = selectedItem.subtype;
 equippedWeapon1Handedness = selectedItem.handedness;
 equippedWeapon1AttackBonus = selectedItem.attackBonus;
 equippedWeapon1DamageType = selectedItem.damageType;
 equippedWeapon1DamageDie = selectedItem.damageDie;
 equippedWeapon1DamageBonus = selectedItem.damageBonus;
 equippedWeapon1Descr = selectedItem.descr;
 equippedWeapon2Id = selectedItem.id;
 equippedWeapon2Name = selectedItem.name;
 equippedWeapon2Amount = selectedItem.amount;
 equippedWeapon2Category = selectedItem.category;
 equippedWeapon2Type = selectedItem.type;
 equippedWeapon2Subtype = selectedItem.subtype;
 equippedWeapon2Handedness = selectedItem.handedness;
 equippedWeapon2AttackBonus = selectedItem.attackBonus;
 equippedWeapon2DamageType = selectedItem.damageType;
 equippedWeapon2DamageDie = selectedItem.damageDie;
 equippedWeapon2DamageBonus = selectedItem.damageBonus;
 equippedWeapon2Descr = selectedItem.descr;
      break;
    
    case "shield":
       equippedShieldId = selectedItem.id;
 equippedShieldName = selectedItem.name;
 equippedShieldAmount = selectedItem.amount;
 equippedShieldCategory = selectedItem.category;
 equippedShieldType = selectedItem.type;
 equippedShieldAcBonus = selectedItem.acBonus;
 equippedShieldDamageType = selectedItem.damageType;
 equippedShieldDamageDie = selectedItem.damageDie;
 equippedShieldDamageBonus = selectedItem.damageBonus;
 equippedShieldDescr = selectedItem.descr;
      break;
      case "helmet":
        equippedHelmetId = selectedItem.id;
equippedHelmetName = selectedItem.name;
equippedHelmetAmount = selectedItem.amount;
equippedHelmetCategory = selectedItem.category;
equippedHelmetType = selectedItem.type;
equippedHelmetChaBonus = selectedItem.chaBonus;
equippedHelmetDescr = selectedItem.descr;
       break;

    case "torso":
      equippedTorsoId = selectedItem.id;
       equippedTorsoName = selectedItem.name;
       equippedTorsoAmount = selectedItem.amount;
       equippedTorsoCategory = selectedItem.category;
       equippedTorsoType = selectedItem.type;
       equippedTorsoSubtype = selectedItem.subtype;
       equippedTorsoAttackBonus = selectedItem.attackBonus;
       equippedTorsoAcBonus = selectedItem.acBonus;
       equippedTorsoWisBonus = selectedItem.wisBonus;
       equippedTorsoIntBonus = selectedItem.intBonus;
       equippedTorsoDescr = selectedItem.descr;
      break;

    case "back":
       equippedBackId = selectedItem.id;
       equippedBackName = selectedItem.name;
       equippedBackAmount = selectedItem.amount;
       equippedBackCategory = selectedItem.category;
       equippedBackType = selectedItem.type;
       equippedBackAcBonus = selectedItem.acBonus;
       equippedBackIntBonus = selectedItem.intBonus;
       equippedBackDescr = selectedItem.descr;
      break;

    case "pants":
       equippedPantsId = selectedItem.id;
 equippedPantsName = selectedItem.name;
 equippedPantsAmount = selectedItem.amount;
 equippedPantsCategory = selectedItem.category;
 equippedPantsType = selectedItem.type;
 equippedPantsAcBonus = selectedItem.acBonus;
 equippedPantsIntBonus = selectedItem.intBonus;
 equippedPantsDescr = selectedItem.descr;
      break;
      case "accessory":
            equippedAccessory1Name = selectedItem.name;
            equippedAccessory1Amount = selectedItem.amount;
            equippedAccessory1Category = selectedItem.category;
            equippedAccessory1Type = selectedItem.type;
            equippedAccessory1IntBonus = selectedItem.intBonus;
            equippedAccessory1Desc = selectedItem.desc;
            equippedAccessory2Name = selectedItem.name;
            equippedAccessory2Amount = selectedItem.amount;
            equippedAccessory2Category = selectedItem.category;
            equippedAccessory2Type = selectedItem.type;
            equippedAccessory2IntBonus = selectedItem.intBonus;
            equippedAccessory2Desc = selectedItem.desc;
            equippedAccessory3Name = selectedItem.name;
            equippedAccessory3Amount = selectedItem.amount;
            equippedAccessory3Category = selectedItem.category;
            equippedAccessory3Type = selectedItem.type;
            equippedAccessory3IntBonus = selectedItem.intBonus;
            equippedAccessory3Desc = selectedItem.desc;
            equippedAccessory4Name = selectedItem.name;
            equippedAccessory4Amount = selectedItem.amount;
            equippedAccessory4Category = selectedItem.category;
            equippedAccessory4Type = selectedItem.type;
            equippedAccessory4IntBonus = selectedItem.intBonus;
            equippedAccessory4Desc = selectedItem.desc;
        break;
    }
        }
    
      
    
    






function unequipItem(slotId, itemId) {
 
  // Check if the item exists before attempting to unequip it
  const unequippedItem = items.find(item => item.id === itemId);
  console.log("Attempting to unequip item with ID:", itemId); // Log the ID of the unequipped item

  if (unequippedItem) {
      // Clear the corresponding equipped slot
      switch (slotId) {
          case "equippedWeapon1":
            equippedWeapon1Id = null;
equippedWeapon1Name = null;
equippedWeapon1Amount = null;
equippedWeapon1Category = null;
equippedWeapon1Type = null;
equippedWeapon1Subtype = null;
equippedWeapon1Handedness = null;
equippedWeapon1AttackBonus = null;
equippedWeapon1DamageType = null;
equippedWeapon1DamageDie = null;
equippedWeapon1DamageBonus = null;
equippedWeapon1Descr = null;
              equippedWeapon1Text.innerHTML = "";
              equippedWeapon1 = null; // Clear the equipped item variable
              break;
          case "equippedWeapon2":
            equippedWeapon2Id = null;
equippedWeapon2Name = null;
equippedWeapon2Amount = null;
equippedWeapon2Category = null;
equippedWeapon2Type = null;
equippedWeapon2Subtype = null;
equippedWeapon2Handedness = null;
equippedWeapon2AttackBonus = null;
equippedWeapon2DamageType = null;
equippedWeapon2DamageDie = null;
equippedWeapon2DamageBonus = null;
equippedWeapon2Descr = null;
equippedShieldId = null;
equippedShieldName = null;
equippedShieldAmount = null;
equippedShieldCategory = null;
equippedShieldType = null;
equippedShieldAcBonus = null;
equippedShieldDamageType = null;
equippedShieldDamageDie = null;
equippedShieldDamageBonus = null;
equippedShieldDescr = null;
              equippedWeapon2Text.innerHTML = "";
              equippedWeapon2 = null; // Clear the equipped item variable
              break;
          case "equippedHelmet":
            equippedHelmetId = null;
equippedHelmetName = null;
equippedHelmetAmount = null;
equippedHelmetCategory = null;
equippedHelmetType = null;
equippedHelmetChaBonus = null;
equippedHelmetDescr = null;
              equippedHelmetText.innerHTML = "";
              equippedHelmet = null; // Clear the equipped item variable
              break;
          case "equippedTorso":
            equippedTorsoId = null;
equippedTorsoName = null;
equippedTorsoAmount = null;
equippedTorsoCategory = null;
equippedTorsoType = null;
equippedTorsoSubtype = null;
equippedTorsoAttackBonus = null;
equippedTorsoAcBonus = null;
equippedTorsoWisBonus = 0;
equippedTorsoIntBonus = 0;
equippedTorsoDescr = null;
              equippedTorsoText.innerHTML = "";
              equippedTorso = null; // Clear the equipped item variable
              break;
          case "equippedBack":
            equippedBackId = null;
equippedBackName = null;
equippedBackAmount = null;
equippedBackCategory = null;
equippedBackType = null;
equippedBackAcBonus = null;
equippedBackIntBonus = null;
equippedBackDescr = null;
              equippedBackText.innerHTML = "";
              equippedBack = null; // Clear the equipped item variable
              break;
          case "equippedPants":
            equippedPantsId = null;
equippedPantsName = null;
equippedPantsAmount = null;
equippedPantsCategory = null;
equippedPantsType = null;
equippedPantsAcBonus = null;
equippedPantsIntBonus = null;
equippedPantsDescr = null;
              equippedPantsText.innerHTML = "";
              equippedPants = null; // Clear the equipped item variable
              break;
              case "equippedAccessory1":
                equippedAccessory1Name = null;
equippedAccessory1Amount = null;
equippedAccessory1Category = null;
equippedAccessory1Type = null;
equippedAccessory1IntBonus = null;
equippedAccessory1Desc = null;
                equippedAccessory1Text.innerHTML = "";
                shiftAccessoriesUp(1); // Shift accessories up starting from slot 1
                equippedAccessory4 = null; // Clear the equipped item variable for slot 4
                break;
              case "equippedAccessory2":
                equippedAccessory2Name = null;
equippedAccessory2Amount = null;
equippedAccessory2Category = null;
equippedAccessory2Type = null;
equippedAccessory2IntBonus = null;
equippedAccessory2Desc = null;
                equippedAccessory2Text.innerHTML = "";
                shiftAccessoriesUp(2); // Shift accessories up starting from slot 2
                equippedAccessory4 = null; // Clear the equipped item variable for slot 4
                break;
              case "equippedAccessory3":
                equippedAccessory3Name = null;
equippedAccessory3Amount = null;
equippedAccessory3Category = null;
equippedAccessory3Type = null;
equippedAccessory3IntBonus = null;
equippedAccessory3Desc = null;
                equippedAccessory3Text.innerHTML = "";
                shiftAccessoriesUp(3); // Shift accessories up starting from slot 3
                equippedAccessory4 = null; // Clear the equipped item variable for slot 4
                break;
              case "equippedAccessory4":
                equippedAccessory4Name = null;
equippedAccessory4Amount = null;
equippedAccessory4Category = null;
equippedAccessory4Type = null;
equippedAccessory4IntBonus = null;
equippedAccessory4Desc = null;
                equippedAccessory4Text.innerHTML = "";
                equippedAccessory4 = null; // Clear the equipped item variable
                break;
          default:
              console.error("Unknown slotId:", slotId);
              break;
      }

      // Check if the unequipped item is a two-handed weapon
      if (unequippedItem.handedness === "two") {
          // Clear the left-hand slot text
          equippedWeapon2Text.innerHTML = "";
          equippedWeapon2 = null; // Clear the equipped item variable
      }

      unequippedItem.amount++;

      equipStatUpdate();
      values();

      // Update the inventory display
      showInventory();

      // Log the unequipped item's details
      console.log(unequippedItem.name + " unequipped");
  } else if (itemId !== "fist") {
      console.error("Unequipped item not found with ID:", itemId);
  }
}



function shiftAccessoriesUp(startingSlot) {
  // Shift accessories up starting from the given slot
  for (let i = startingSlot; i < 4; i++) {
    const currentSlotText = document.querySelector("#equippedAccessory" + i);
    const nextSlotText = document.querySelector("#equippedAccessory" + (i + 1));

    if (nextSlotText.innerHTML !== "") {
      // If the next slot is filled, move its content to the current slot
      currentSlotText.innerHTML = nextSlotText.innerHTML;
      // Clear the next slot
      nextSlotText.innerHTML = "";
    } else {
      // If the next slot is empty, clear the current slot
      currentSlotText.innerHTML = "";
      break; // No need to continue shifting if we've reached an empty slot
    }
  }
}



function equipStatUpdate(){
  ac = 10 + equippedShieldAcBonus + equippedTorsoAcBonus + equippedBackAcBonus + equippedPantsAcBonus;
  acText.innerHTML = ac;
  str = 1 + strBonus;
  strText.innerHTML = str;
  dex = 1 + dexBonus;
  dexText.innerHTML = dex;
  con = 1 + conBonus;
  conText.innerHTML = con;
  int = 1 + equippedTorsoIntBonus;
  intText.innerHTML = int;
  wis = 1 + equippedTorsoWisBonus;
  wisText.innerHTML = wis;
  cha = 3 + chaBonus;
  chaText.innerHTML = cha;
  }




function calcAtk(){
  let attackRollCalc = dXRoll(20);
  atkRoll.innerHTML = attackRollCalc;
  attackRollCalc = Math.floor(str/2) + attackRollCalc + equippedWeapon1AttackBonus;
  console.log(attackRollCalc + " attack roll");
  return attackRollCalc;
}

function calcDam(){
  let damageRoll = dXRoll(equippedWeapon1DamageDie);
  damRoll.innerHTML = damageRoll;
  console.log(damageRoll + " damage rolled in calcDam");

  // Calculate damage without negative values
  let damage = Math.max(Math.floor(str/2) + damageRoll + equippedWeapon1DamageBonus, 0);

  console.log(damage + " damage calculated in calcDam");
  return damage;
}



function equipItemStart(i){
  equippedWeapon1Id = items[i].id;
  equippedWeapon1 = items[i].name;
  equippedWeapon1Type = items[i].type;
  equippedWeapon1AttackBonus = items[i].attackBonus;
  equippedWeapon1DamageType = items[i].damageType;
  equippedWeapon1DamageDie = items[i].damageDie;
  equippedWeapon1DamageBonus = items[i].damageBonus;
  equippedWeapon1Handedness = items[i].handedness;
 
  equippedWeapon1Text.innerHTML = items[i].name;
 
 items[i].amount --;
 if(oldId != null){
   items[oldId].amount ++;
 }
 
 oldId = i;
 
 }


